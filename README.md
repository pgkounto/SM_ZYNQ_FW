# APOLLO Service Module firmware

## Github branching policy:
We are going to try to follow: https://nvie.com/posts/a-successful-git-branching-model/
The default branch is develop and you should branch off of that.

## Building
The Apollo SM has several different configurations and this build system can be instructed to make any of them.
The differences between these configurations are located in the "configs" directory.
The build process uses Vivado but is directed by makefiles. 
Type "make list" for a list of builds
```bash
make list

Apollo CM config:
rev1_xc7z035	rev1_xc7z045	rev2a_xczu7ev	rev2_xc7z035	rev2_xc7z045	rev2_xczu7ev

Prebuilds:
prebuild_rev1_xc7z035	prebuild_rev2a_xczu7ev	prebuild_rev2_xc7z045	prebuild_xc7z035
prebuild_rev1_xc7z045	prebuild_rev2_xc7z035	prebuild_rev2_xczu7ev

Vivado:
open_hw		open_impl	open_project	open_synth

Clean:
clean_address_tables	clean_bd		clean_CM		clean_ip		clean_make_log		clean_remote
clean_autogen					clean_bit					clean_everything	clean_kernel  clean_prebuild
```

### configs
The configs directory contains directories for each different configuraiton and the name of this directory is used by make for the make rule
In these folders you will need to have atleast the following three files:
 - files.tcl  
   A tcl file that lists the files to be included
   There are
   - bd_files   : the list of BD names and tcl files to be built
   - vhdl_files : the list of VHDL files this project uses
   - xdc_files  : the list of xdc files for the project
   - xci_files  : the list of IP core xci files used
 - settings.tcl  
   This file primarily lists the FPGA part number
 - config.yaml 
   Lists AXI slaves that exist or should be made, their AXI address info, uHAL address/tables, and if they should auto generate AXI slave decoding logic from the uHAL address tables
   This also can contain IPCores that are to be generated for this build. 
 - autogen
   Where autogenerated HDL decoders are located.   
 - autogen/cores
   Where autogenerated IPCores are located 
#### Additional files

     This is where customized top files should be put, but that is not a requirement. 
     This is a good place to put any other config specific files, but it is not a requirement. 
     These files used are always taken from the files.tcl (with the exception of the autogenerated FW info hdl file)

### Building
As mentioned above, make is used to drive the build.  There are three parts to this build:
 - FPGA FW build 
   Located in the main directory (with this file)
   Builds the Zynq FPGA FW   
 - FSBL/uboot/Linux kernel build
   Located in ./kernel
   Builds the files needed to boot the Zynq PS+PL
 - CentOS FS build
   Located in ./os
   Builds the CentOS filesystem used.  
   Requires sudo privilages (for now)

#### Make
Buildable Groups:
  > make list

    - Apollo SM config:
      Different FWs to make (you probably just want one of these)
    - Prebuild:
      provides address table to HDL conversions, kernel & OS yaml files, and AddSlaves.tcl building
    - Vivado:
      Drive interactive vivado sessions
    - Clean:
      Clean different parts of the builds
    - Tests:
      Test benches run and compared against golden outputs
    - Test-benches:
      Start interactive test-benches

#### FW
To Build FPGA FW:
  `make revN_xcFPGA`

  Ouput:
  
   - bit/top_revN_xcFPGA.bit
  
   - kernel/hw/*.dtsi_chunk,*.dtsi_post_chunk,hwdef

To Build zynq fsbl+kernel+fs
  
  - First, build FPGA FW
    `make revN_xcFPGA`
  - `cd kernel`
    `make revN_xcFPGA`

  Output:
  
   - kernel/zynq_os/images/linux/BOOT.bin
  
   - kernel/zynq_os/images/linux/image.ub


To Build the centos image
   - First, build FPGA FW if you haven't already
   - `cd os`
   - `sudo make clean; sudo make revN_xcFPGA.tar.gz`

   Output: (follow os/README for copying instructions)   
   - os/image/revN_xcFPGA.tar.gz
   - os/image/revN_xcFPGA/

   
### Organization:
  Build scripts are in ./scripts and are called by the Makefile
  
  Zynq block diagram generation tcl scripts are in ./src/ZynqOS
    build_Zynq_revN_xcFPGA.tcl is automatically called by build scripts.
  This relies on the tcl scripts in the submodule in bd.

  Build specific HDL & constraint files are usuall in ./config/revN_xcFPGA/src
  HDL used in all builds is in ./src/
  config.yaml in ./config lists the slaves to be built and the tcl needed to build them
  Autogenerated HDL _map.vhd and _PKG.vhd are located in ./config/revN_xcFPGA/autogen and are committed to git for record keeping
  Autogenerated IP Cores are in ./config/revN_xcFPGA/autogetn/cores and are not added to git.

  Linux Kernel & FSBL:
    ./kernel/hw contains device-tree elements and the xilinx hwdef files needed to build the PS system
    ./kernel/zynq_os_mods contains recipes for mods/patches for/of the petalinux system.

  CentOS:
    ./os/address_table contains the build address table and module files
    ./os/configs/revN contains the modifications to the centos file system.
    ./os/scripts and ./os/mk contain the build rules and scripts for the filesystem creation.

### Dependencies:
	generation of xml regmaps requires the Jinja2 library for python
